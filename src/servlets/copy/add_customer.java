package servlets.copy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Dao;

/**
 * Servlet implementation class add_customer
 */
@WebServlet("/add_customer")
public class add_customer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public add_customer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String organization = request.getParameter("organization");
		String phonenumber = request.getParameter("phonenumber");
		String email = request.getParameter("email");
	    String password = request.getParameter("password");


	    response.setContentType("text/html");

	    PrintWriter out = response.getWriter();

	    ResultSet rs = Dao.executeQuery("INSERT INTO customers(firstname, lastname, organization, phonenumber, email, password) VALUES (" +"\'" + firstname + "\', \'" + lastname + "\', \'" + organization + "\', \'" + phonenumber + "\', \'" + email + "\', \'" + password+ "\')");
	
	    out.println("<!DOCTYPE html>\n" + 
	    		"<html>\n" + 
	    		"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + 
	    		"\n" + 
	    		"<link href=\"https://fonts.googleapis.com/css2?family=Varta:wght@500&display=swap\" rel=\"stylesheet\">\n" + 
	    		"<!-- make background color of webpage gray -->\n" + 
	    		"<style>\n" + 
	    		"  body {\n" + 
	    		"    background-color: #798591;\n" + 
	    		"    margin:0px;\n" + 
	    		"  }\n" + 
	    		"\n" + 
	    		"  input[type=submit] {\n" + 
	    		"    background-color: #3BB44B;\n" + 
	    		"    color: white;\n" + 
	    		"    padding: 12px 20px;\n" + 
	    		"    border: none;\n" + 
	    		"    border-radius: 4px;\n" + 
	    		"    cursor: pointer;\n" + 
	    		"    font-family:Varta;\n" + 
	    		"  }\n" + 
	    		"\n" + 
	    		"  input[type=submit]:hover {\n" + 
	    		"    background-color: #45a049;\n" + 
	    		"  }\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"<!-- top menu bar-->\n" + 
	    		"<div style=\"background-color: white; top: 0px; width:100%; margin:0px; padding: 0px; height:60px; border:0px solid #001; position:fixed; z-index:1;\">\n" + 
	    		"\n" + 
	    		"  <!-- ********************************************************************************************************************************************** -->\n" + 
	    		"  <!-- Logo -->\n" + 
	    		"  <a href = \"Home Page.html\">\n" + 
	    		"  <img style = \"position:fixed; height:60px; left:15px;\" src=\"ServiceCorp Logo.png\"/>\n" + 
	    		"  </a>\n" + 
	    		"\n" + 
	    		"  <!-- log in button on header-->\n" + 
	    		"  <a style = \"font-family:Varta; text-align: right; right:60px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
	    		"  href = \"Customer Login Page.html\"> customers </a>\n" + 
	    		"\n" + 
	    		"  <h2 style = \"font-family:Varta; text-align: right; right:190px; position:fixed; color:#5E5C5F; opacity: 0.2; font-size:20px;\"> | </h2>\n" + 
	    		"  <a style = \"font-family:Varta; text-align: right; right:250px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
	    		"  href = \"Employee Login Page.html\"> employees </a>\n" + 
	    		"  <h2 style = \"font-family:Varta; text-align: right; right:390px; position:fixed; color:#5E5C5F; opacity: 0.2; font-size:20px;\"> | </h2>\n" + 
	    		"  <a style = \"font-family:Varta; text-align: right; right:450px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
	    		"  href = \"The Team.html\"> the team </a>\n" + 
	    		"  <!-- ********************************************************************************************************************************************** -->\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"</div>\n" + 
	    		"<p style = \"padding:70px;\"></p>\n" + 
	    		"\n" + 
	    		"<p style = \"font-size:15px; font-family:Abel; padding:0px; color:white; line-height: 2; position:absolute; bottom:0px; left:30px;\n" + 
	    		"color:white; line-height: 1.75 \">\n" + 
	    		"</p>\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"<!-- Background gray box -->\n" + 
	    		"<!-- ********************************************************************************************************************************************** -->\n" + 
	    		"<form action = \"Customer Portal.jsp\">\n" + 
	    		"<!-- ********************************************************************************************************************************************** -->\n" + 
	    		"<div style=\"background-color: #f2f2f2; position: relative; width:80%; margin-left:auto; margin-right:auto; padding: 0px;\n" + 
	    		"border:0px solid #001; font-family:Varta; color:#798591; text-align:center; border-radius: 5px;\">\n" + 
	    		"<br>\n" + 
	    		"<h2>Registration complete!</h2>\n" + 
	    		"<p>Thank you for your business, you may now make a work order request.</p>\n" + 
	    		"\n" + 
	    		"<!-- ********************************************************************************************************************************************** -->\n" + 
	    		"<input type=\"submit\" value=\"SUBMIT AN ORDER\">\n" + 
	    		"<!-- ********************************************************************************************************************************************** -->\n" + 
	    		"<p style = \"padding:10px;\"></p>\n" + 
	    		"</div>\n" + 
	    		"</form>\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"<!-- Footer -->\n" + 
	    		"<p style = \"padding:300px;\"></p>\n" + 
	    		"<div style=\"background-color: white; bottom: 0px; width:100%; margin:0px; padding: 0px; height:100px; border:0px solid #001; position:relative; z-index:1; color:#5E5C5F;\">\n" + 
	    		"<p style = \"font-family:Varta; font-size:20px; margin-left:30px; padding-top:10px; margin-bottom:0px;\">Contact Us</p>\n" + 
	    		"<p style = \"font-family:Varta; font-size:12px; margin-left:55px; margin-top:0px; line-height:2;\">Phone: (711) 265-9193<br>Email: support@sevicecorps.com</p>\n" + 
	    		"<p style = \"font-family:Varta; font-size:12px; top:-25px; margin-top:-15px; text-align:right; margin-right:30px;\">ServiceCorp©</p>\n" + 
	    		"</div>\n" + 
	    		"\n" + 
	    		"\n" + 
	    		"</html>");
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
