package servlets.copy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Dao;
import servicecorp.work_order;

/**
 * Servlet implementation class display_orders
 */
@WebServlet("/display_orders")
public class display_orders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public display_orders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			ResultSet rs = Dao.getAll("work_orders");
			ArrayList<work_order> allWorkOrders = new ArrayList<work_order>();
			response.setContentType("text/html");
			while (rs.next()) {
				work_order current = new work_order(rs.getInt("work_orders_id"), rs.getDate("order_date"), rs.getString("printer"), rs.getString("location"), rs.getString("description"), rs.getString("customer_email"));
				allWorkOrders.add(current);
			}
			
			PrintWriter out1 = response.getWriter();
			
			out1.println("<!DOCTYPE html>\n" + 
					"<html>\n" + 
					"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + 
					"<link href=\"https://fonts.googleapis.com/css2?family=Varta:wght@500&display=swap\" rel=\"stylesheet\">\n" + 
					"<!-- make background color of webpage gray -->\n" + 
					"<style>\n" + 
					"  body {\n" + 
					"    background-color: #798591;\n" + 
					"    margin:0px;\n" + 
					"  }\n" + 
					"</style>\n" + 
					"\n" + 
					"\n" + 
					"<!-- top menu bar-->\n" + 
					"<div style=\"background-color: white; top: 0px; width:100%; margin:0px; padding: 0px; height:60px; border:0px solid #001; position:fixed; z-index:1;\">\n" + 
					"\n" + 
					"  <!-- ********************************************************************************************************************************************** -->\n" + 
					"  <!-- Logo -->\n" + 
					"  <a href = \"Home Page.html\">\n" + 
					"  <img style = \"position:fixed; height:60px; left:15px;\" src=\"ServiceCorp Logo.png\"/>\n" + 
					"  </a>\n" + 
					"\n" + 
					"  <!-- log in button on header-->\n" + 
					"  <a style = \"font-family:Varta; text-align: right; right:60px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
					"  href = \"Customer Login Page.html\"> customers </a>\n" + 
					"\n" + 
					"  <h2 style = \"font-family:Varta; text-align: right; right:190px; position:fixed; color:#5E5C5F; opacity: 0.2; font-size:20px;\"> | </h2>\n" + 
					"  <a style = \"font-family:Varta; text-align: right; right:250px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
					"  href = \"Employee Login Page.html\"> employees </a>\n" + 
					"  <h2 style = \"font-family:Varta; text-align: right; right:390px; position:fixed; color:#5E5C5F; opacity: 0.2; font-size:20px;\"> | </h2>\n" + 
					"  <a style = \"font-family:Varta; text-align: right; right:450px; position:fixed; color:#5E5C5F; font-size:20px; text-decoration: none; top:15px;\"\n" + 
					"  href = \"The Team.html\"> the team </a>\n" + 
					"  <!-- ********************************************************************************************************************************************** -->\n" + 
					"\n" + 
					"\n" + 
					"</div>\n" + 
					"<p style = \"padding:70px;\"></p>\n" + 
					"\n" + 
					"\n" + 
					"\n" + 
					"\n" + 
					"<!-- username and password input boxe formatting in first input stanza, green submit button in second input stanza-->\n" + 
					"<style>\n" + 
					"input[type=text], select {\n" + 
					"  padding: 12px 50px;\n" + 
					"  margin: 8px 0;\n" + 
					"  margin-left: -20px;\n" + 
					"  border: 1px solid #ccc;\n" + 
					"  border-radius: 4px;\n" + 
					"  box-sizing: border-box;\n" + 
					"}\n" + 
					"\n" + 
					"input[type=submit] {\n" + 
					"  margin-left:auto;\n" + 
					"  margin-right:auto;\n" + 
					"  width: 80%;\n" + 
					"  background-color: #3BB44B;\n" + 
					"  color: white;\n" + 
					"  padding: 14px 20px;\n" + 
					"  margin: 8px 0;\n" + 
					"  border: none;\n" + 
					"  border-radius: 4px;\n" + 
					"  cursor: pointer;\n" + 
					"  transform:translateX(30%)\n" + 
					"}\n" + 
					"\n" + 
					"input[type=submit]:hover {\n" + 
					"  background-color: #45a049;\n" + 
					"}\n" + 
					"\n" + 
					"div {\n" + 
					"  margin-left:auto;\n" + 
					"  margin-right:auto;\n" + 
					"  width:80%;\n" + 
					"  border-radius: 5px;\n" + 
					"  background-color: #f2f2f2;\n" + 
					"  padding: 20px;\n" + 
					"}\n" + 
					"</style>\n" + 
					"<body>\n" + 
					"\n" + 
					"<div>\n" + 
					"  <style>\n" + 
					"  label{\n" + 
					"    width: 20%;\n" + 
					"    display: inline-block;\n" + 
					"    text-align: left;\n" + 
					"    margin-left:90px;\n" + 
					"    padding-right:50px;\n" + 
					"    vertical-align:top;\n" + 
					"    font-family:Varta;\n" + 
					"    margin-top:10px;\n" + 
					"  }\n" + 
					"  select{\n" + 
					"    width: 80%;\n" + 
					"    display:inline-block;\n" + 
					"    font-family:Varta;\n" + 
					"  }\n" + 
					"\n" + 
					"  input[type =text]{\n" + 
					"    width: 80%;\n" + 
					"    display:inline-block;\n" + 
					"    font-family:Varta;\n" + 
					"  }\n" + 
					"  input[type =radio]{\n" + 
					"    margin-left:-160px;\n" + 
					"    font-family:Varta;\n" + 
					"  }\n" + 
					"\n" + 
					"table, th, td {\n" + 
					"  font-family:Varta;\n" +
					"  padding: 25px;\n" +
					"  border: 1px solid black;\n" + 
					"  border-collapse: collapse;\n" + 
					"  width: 100%;\n" +
					"}" +
					"  </style>\n" + 
					"\n" + 
					"    <form>\n" + 
					"      <h1 style = \"font-family:Varta; color:#798591;\"> Employee Portal - Opened Orders</h1>\n");
			
			
			StringBuilder out = new StringBuilder();
			out.append("<table style=\"width:100%\"><tr>");
			out.append("<th>Order ID</th>");
			out.append("<th>Order Date</th>");
			out.append("<th>Printer Model</th>");
			out.append("<th>Location</th>");
			out.append("<th>Description of Issue</th>");
			
			for (work_order w:allWorkOrders) {
				out.append(w.toTableRow());
			}
			
			out.append("</table>");
			response.getWriter().append(out.toString());
			
			
			out1.println("    </form>\n" + 
					"</div>\n" + 
					"\n" + 
					"<!-- Footer -->\n" + 
					"<p style = \"padding:300px;\"></p>\n" + 
					"<div style=\"background-color: white; bottom: 0px; width:100%; margin:0px; padding: 0px; height:100px; border:0px solid #001; position:relative; z-index:1; color:#5E5C5F;\">\n" + 
					"<p style = \"font-family:Varta; font-size:20px; margin-left:30px; padding-top:10px; margin-bottom:0px;\">Contact Us</p>\n" + 
					"<p style = \"font-family:Varta; font-size:12px; margin-left:55px; margin-top:0px; line-height:2;\">Phone: (711) 265-9193<br>Email: support@sevicecorps.com</p>\n" + 
					"<p style = \"font-family:Varta; font-size:12px; top:-25px; margin-top:-15px; text-align:right; margin-right:30px;\">ServiceCorpÂ©</p>\n" + 
					"</div>\n" + 
					"</html>\n" + 
					"");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
