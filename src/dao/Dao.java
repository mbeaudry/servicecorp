package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class Dao {
	private static String uri = "jdbc:postgresql://localhost:5432/ServiceCorp";
	private static String db_name = "ServiceCorp";
	private static String username = "postgres";
	private static String password = "psql";
	
	public static Connection connect() {
		Connection conn = null;
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(uri, username, password);
			System.out.println("Connected to PostgreSQL server successfully");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	public static ResultSet executeQuery(String query) {
		//System.out.println("Begin execute query: " + query);
		ResultSet rs = null;
		try {
			Connection conn = connect();
			//Statement stmt = conn.createStatement();
			PreparedStatement prep = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = prep.executeQuery();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}
	
	public static ResultSet getAll(String tableName) {
		return executeQuery("SELECT * FROM " + tableName + ";");
	}
	
}