package servicecorp;

public class invoice {
	public int order_id;
	public float price;
	public boolean paid;
	
	public invoice(int order_id, float price, boolean paid) {
		this.set_order_id(order_id);
		this.set_price(price);
		this.set_paid(paid);
	}
	
	public void set_order_id(int order_id) {
		this.order_id = order_id;
	}
	
	public void set_price(float price) {
		this.price = price;
	}
	
	public void set_paid(boolean paid) {
		this.paid = paid;
	}
	
	public int get_order_id() {
		return this.order_id;
	}
	
	public float get_price() {
		return this.price;
	}
	
	public boolean get_paid() {
		return this.paid;
	}
	
	public String toTableRow() {
		StringBuilder out = new StringBuilder();
		out.append("<tr>");
		out.append("<td>" + this.get_order_id() + "</td>");
		out.append("<td>" + this.get_price() + "</td>");
		out.append("<td>" + this.get_paid() + "</td>");
		out.append("</tr>");
		return out.toString();
	}

}
